package com.afs.restapi;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.minidev.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class EmployeeControllerTest {

    @Autowired
    private MockMvc client;

    @Autowired
    private EmployeeRepository repository;

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        repository.clearAll();
    }

    @Test
    void should_return_employees_when_perform_get_given_employees_in_repository() throws Exception {
        // given
        Employee employeeNevin = new Employee(1L, "Nevin", 18, "Male", 1800);
        Employee employeeZeng = new Employee(2L, "Zeng", 18, "Male", 2800);
        repository.insert(employeeNevin);
        repository.insert(employeeZeng);

        // when   then
//        client.perform(MockMvcRequestBuilders.get("/employees"))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$", hasSize(2)))
//                .andExpect(jsonPath("$[0].id").isNumber())
//                .andExpect(jsonPath("$[0].id").value(1))
//                .andExpect(jsonPath("$[0].name").isString())
//                .andExpect(jsonPath("$[0].name").value("Nevin"))
//                .andExpect(jsonPath("$[0].age").isNumber())
//                .andExpect(jsonPath("$[0].age").value(18))
//                .andExpect(jsonPath("$[1].id").isNumber())
//                .andExpect(jsonPath("$[1].id").value(2))
//                .andExpect(jsonPath("$[1].name").isString())
//                .andExpect(jsonPath("$[1].name").value("Zeng"))
//                .andExpect(jsonPath("$[1].age").isNumber())
//                .andExpect(jsonPath("$[1].age").value(18));

        client.perform(MockMvcRequestBuilders.get("/employees"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(content().json(objectMapper.writeValueAsString(List.of(employeeNevin, employeeZeng))));
    }

    @Test
    void should_return_right_employees_when_perform_get_by_id_given_employees_in_repository_and_right_id() throws Exception {
        // given
        Employee employeeNevin = new Employee(1L, "Nevin", 18, "Male", 1800);
        Employee employeeZeng = new Employee(2L, "Zeng", 18, "Male", 2800);
        repository.insert(employeeNevin);
        repository.insert(employeeZeng);

        // when   then
        client.perform(MockMvcRequestBuilders.get("/employees/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(employeeNevin)));
    }

    @Test
    void should_return_male_employees_when_perform_get_by_gender_given_employees_in_repository_and_right_gender() throws Exception {
        // given
        Employee employeeNevin = new Employee(1L, "Nevin", 18, "Male", 1800);
        Employee employeeZeng = new Employee(2L, "Zeng", 18, "Male", 2800);
        Employee employeeLiu = new Employee(3L, "Liu", 18, "Female", 2800);
        repository.insert(employeeNevin);
        repository.insert(employeeZeng);
        repository.insert(employeeLiu);

        // when   then
        client.perform(MockMvcRequestBuilders.get("/employees").param("gender", "Male"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(content().json(objectMapper.writeValueAsString(List.of(employeeNevin, employeeZeng))));
    }

    @Test
    void should_return_employees_list_by_page_when_perform_get_by_page_given_employees_in_repository_and_page_and_size() throws Exception {
        // given
        Employee employeeNevin = new Employee(1L, "Nevin", 18, "Male", 1800);
        Employee employeeZeng = new Employee(2L, "Zeng", 18, "Male", 2800);
        Employee employeeLiu = new Employee(3L, "Liu", 18, "Female", 2800);
        Employee employeeWang= new Employee(4L, "Wang", 18, "Female", 2800);
        repository.insert(employeeNevin);
        repository.insert(employeeZeng);
        repository.insert(employeeLiu);
        repository.insert(employeeWang);

        // when   then
        client.perform(MockMvcRequestBuilders.get("/employees")
                        .param("page", String.valueOf(2))
                        .param("size", String.valueOf(2)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(content().json(objectMapper.writeValueAsString(List.of(employeeLiu, employeeWang))));
    }

    @Test
    void should_return_employee_and_repository_saved_it_when_perform_post_given_employee_info() throws Exception {
        // given
        Employee employeeNevin = getEmployeeNevin();
        String employeeJson = objectMapper.writeValueAsString(employeeNevin);

        // when then
        client.perform(MockMvcRequestBuilders.post("/employees")
                .contentType(MediaType.APPLICATION_JSON)
                .content(employeeJson))
                .andExpect(status().isCreated())
                .andExpect(content().json(objectMapper.writeValueAsString(employeeNevin)));
        assertEquals(employeeNevin.getId(), 1);
        assertEquals(employeeNevin.getName(), "Nevin");
        assertEquals(employeeNevin.getAge(), 18);
    }

    @Test
    void should_return_employee_and_updated_it_when_perform_put_given_employee_info_and_id() throws Exception {
        //given
        Employee employeeNevin = getEmployeeNevin();
        repository.insert(employeeNevin);
        Employee toBeUpdateNevin = getEmployeeNevin();
        toBeUpdateNevin.setSalary(20000);
        String nevinJson = objectMapper.writeValueAsString(toBeUpdateNevin);

        //when
        client.perform(MockMvcRequestBuilders.put("/employees/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(nevinJson))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(toBeUpdateNevin)));
        Employee nevinInRepo = repository.findById(1L);
        assertEquals(toBeUpdateNevin.getSalary(), nevinInRepo.getSalary());
    }

    private Employee getEmployeeNevin() {
        return new Employee(1L, "Nevin", 18, "Male", 1800);
    }

    private Employee getEmployeeZeng() {
        return new Employee(2L, "Zeng", 18, "Male", 1800);
    }
}